/*
 * objectSize.cpp
 * Author: Philip Karunakaran, Immanuel Rajkumar
 * Program to calibrate the camera and compute the size of an object using openCV
 */

#include <cv.h>
#include <highgui.h>
#include <calib3d.hpp>

#define HEIGHT 7 // Chessboard rows
#define WIDTH 10 // Chessboard Columns
#define N 5 // Various orientation count
#define SKIP 20 //Frames to be skiped so the chessboard orientation can be changed

#define OBJ_DIST 30 //in cms - Distance at which calibration object and real world object is placed

using namespace cv;

/*
 * Class to calibrate the camera and identify the focal length of the same;
 * Identify the size of an object after finding the focal length;
 */
class ObjectSize {

private:
	Mat frame, HSVFrame, binaryFrame, opened, closed;
	/* Chess board properties */
	int board_ht; //Rows
	int board_wd; //Columns
	int board_n; //Number of interior corners
	int board_dt; //Boards to be skipped during chessboard orientation change
	int n_boards; //Number of board orientations that will be used for calibration; Higher the better;
	CvSize board_sz; //Board size

	/*Lens settings */
	float fx, fy;

	/*Object size */
	float objx, objy;

public:
	/*
	 * Initialize various board parameters
	 */
	ObjectSize() {
		board_ht = HEIGHT;
		board_wd = WIDTH;
		board_n = (board_ht - 1) * (board_wd - 1);
		n_boards = N;
		board_dt = SKIP;
		board_sz = cvSize(board_wd - 1, board_ht - 1);
		fx = fy = 0;
		objx = objy = 0;
	}

	/*
	 * Function to calibrate the camera
	 * Captures valid set of chessboard orientations using cvFindChessboardCorners;
	 * Orientations fed as i/p to cvCalibrateCamera2 functions
	 */
	int calibrate(void) {
		CvCapture* cap;

		std::cout << "Calibrating the camera\n";
		CvMat* image_points = cvCreateMat(n_boards * board_n, 2, CV_32FC1);
		CvMat* object_points = cvCreateMat(n_boards * board_n, 3, CV_32FC1);
		CvMat* point_counts = cvCreateMat(n_boards, 1, CV_32SC1);
		CvMat* intrinsic_matrix = cvCreateMat(3, 3, CV_32FC1);
		CvMat* distortion_coeffs = cvCreateMat(5, 1, CV_32FC1);
		CvPoint2D32f* corners = new CvPoint2D32f[board_n];
		int corner_count;
		int successes = 0;
		int step, frame = 0;

		cap = cvCreateCameraCapture(0);

		IplImage *image = cvQueryFrame(cap);
		IplImage *gray_image = cvCreateImage(cvGetSize(image), 8, 1);

		while (successes < n_boards) {
			if (frame++ % board_dt == 0) {
				int found = cvFindChessboardCorners(image, board_sz, corners,
						&corner_count,
						CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);
				if (!found) {
					std::cout << "\nNot Found";
				} else {
					std::cout << "\nFound - Computing";
					cvCvtColor(image, gray_image, CV_BGR2GRAY);
					cvFindCornerSubPix(gray_image, corners, corner_count,
							cvSize(11, 11), cvSize(-1, -1), cvTermCriteria(
							CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));

					cvDrawChessboardCorners(image, board_sz, corners,
							corner_count, found);
					if (corner_count == board_n) {
						step = successes * board_n;
						for (int i = step, j = 0; j < board_n; ++i, ++j) {
							CV_MAT_ELEM(*image_points, float,i,0) =
									corners[j].x;
							CV_MAT_ELEM(*image_points, float,i,1) =
									corners[j].y;
							CV_MAT_ELEM(*object_points,float,i,0) = j
									/ board_wd;
							CV_MAT_ELEM(*object_points,float,i,1) = j
									% board_wd;
							CV_MAT_ELEM(*object_points,float,i,2) = 0.0f;
						}
						CV_MAT_ELEM(*point_counts, int,successes,0) = board_n;
						successes++;
					}
				}
				cvShowImage("Calibration", image);
			}

			if (waitKey(30) >= 0)
				return 0;
			image = cvQueryFrame(cap); //Get next image
		}

		std::cout << "\nRequired Orientations obtained";

		/* Calling calibrateCamera2 with above found parameters
		 * cvCalibrateCamera2(object_points2, image_points2,point_counts2, cvGetSize( image ),intrinsic_matrix, distortion_coeffs,NULL, NULL,0 //CV_CALIB_FIX_ASPECT_RATIO);
		 * Compute the focal length from the resultant of calibrateCamera - The intrinsic matrix above also called as camera matrix will have fx and fy - focal length
		 */

		CvMat* object_points2 = cvCreateMat(successes * board_n, 3, CV_32FC1);
		CvMat* image_points2 = cvCreateMat(successes * board_n, 2, CV_32FC1);
		CvMat* point_counts2 = cvCreateMat(successes, 1, CV_32SC1);
		//Transfer the above points onto correct size matrices

		for (int i = 0; i < successes * board_n; ++i) {
			CV_MAT_ELEM( *image_points2, float, i, 0) = CV_MAT_ELEM(
					*image_points, float, i, 0);
			CV_MAT_ELEM( *image_points2, float,i,1) = CV_MAT_ELEM(*image_points,
					float, i, 1);
			CV_MAT_ELEM(*object_points2, float, i, 0) = CV_MAT_ELEM(
					*object_points, float, i, 0);
			CV_MAT_ELEM( *object_points2, float, i, 1) = CV_MAT_ELEM(
					*object_points, float, i, 1);
			CV_MAT_ELEM( *object_points2, float, i, 2) = CV_MAT_ELEM(
					*object_points, float, i, 2);
		}
		for (int i = 0; i < successes; ++i) {
			CV_MAT_ELEM( *point_counts2, int, i, 0) = CV_MAT_ELEM(*point_counts,
					int, i, 0);
		}
		cvReleaseMat(&object_points);

		cvReleaseMat(&image_points);
		cvReleaseMat(&point_counts);

		/*At this point we have all of the chessboard corners we need.
		 *Initialize the intrinsic matrix such that the two focal lengths have a ratio of 1.0
		 */
		CV_MAT_ELEM( *intrinsic_matrix, float, 0, 0 ) = 1.0f;
		CV_MAT_ELEM( *intrinsic_matrix, float, 1, 1 ) = 1.0f;

		/* Calibrating the camera */
		cvCalibrateCamera2(object_points2, image_points2, point_counts2,
				cvGetSize(image), intrinsic_matrix, distortion_coeffs,
				NULL, NULL, 0);

		std::cout << "\nCamera Matrix is: \n";
		for (int i = 0; i < 3; i++) {
			printf("%f %f %f\n", CV_MAT_ELEM(*intrinsic_matrix, float, i, 0),
					CV_MAT_ELEM(*intrinsic_matrix, float, i, 1),
					CV_MAT_ELEM(*intrinsic_matrix, float, i, 2));

		}

		fx = CV_MAT_ELEM(*intrinsic_matrix, float, 0, 0);
		fy = CV_MAT_ELEM(*intrinsic_matrix, float, 1, 1);

		std::cout << "\nFound Focal length";
		cvReleaseCapture(&cap);

		return 0;

	}

	/*
	 * Function that captures the image of an object and computes its size
	 */
	int computeSize(void) {
		VideoCapture cap(0);
		if (!cap.isOpened()) {
			std::cout << "Unable to open the camera";
			return 0;
		}
		namedWindow("Output", 1);

		for (int i = 0;; i++) {
			cap >> frame;
			cvtColor(frame, HSVFrame, CV_BGR2HSV); //converting the image to hsv frame

			/* Identifying objects based on colors - Color Used - Orange
			 * NOTE: inRange converts an image to binary based on the range.
			 * Those in the defined range will be white and the others black
			 */
			inRange(HSVFrame, Scalar(0, 180, 180), Scalar(22, 255, 255),
					binaryFrame);

			Mat element5(5, 5, CV_8U, cv::Scalar(1));
			/* Opening - Erosion and then dilation */
			cv::morphologyEx(binaryFrame, opened, cv::MORPH_OPEN, element5);
			/* Closing - Dilation and then erosion */
			cv::morphologyEx(opened, closed, cv::MORPH_CLOSE, element5);

			/* Find various contours based on connectedness property */
			vector<std::vector<Point> > contours;
			findContours(closed, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);

			vector<Rect> boundRect(contours.size());

			for (size_t i = 0; i < contours.size(); i++)
				boundRect[i] = boundingRect(Mat(contours[i]));

			/* Display the contours and boundary rectangles */
			RNG rng(12345);
			for (size_t i = 0; i < contours.size(); i++) {
				Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255),
						rng.uniform(0, 255));
				drawContours(closed, contours, i, color, 1, 8, vector<Vec4i>(),
						0, Point());
				rectangle(closed, boundRect[i].tl(), boundRect[i].br(), color,
						2, 8, 0);
			}

			int maxX = 0, maxY = 0; //length and width of object
			int x = 0, y = 0;

			// Count max white pixels in any row
			for (int i = 0; i < closed.rows; ++i) {
				x = 0;
				for (int j = 0; j < closed.cols; ++j)
					if (closed.at<uchar>(i, j) != 0)
						x++;
				if (x > maxX)
					maxX = x;
			}

			//Flip rows and cols - to count max white pixels in any cols
			std::cout << "\n\n";
			for (int i = 0; i < closed.cols; ++i) {
				y = 0;
				for (int j = 0; j < closed.rows; ++j)
					if (closed.at<uchar>(j, i) != 0)
						y++;
				if (y > maxY)
					maxY = y;
			}

			//Calculating width D/F = OBJX/maxX. Similarly height
			if (fx != 0)
				objx = (maxX * OBJ_DIST / fx);

			if (fy != 0)
				objy = (maxY * OBJ_DIST / fy);

			std::cout << "\nComputed Width X Height : " << objx << "  X  "
					<< objy << " cms";
			imshow("Output", closed);

			if (waitKey(30) >= 0)
				return 0;
		}
		return 0;
	}
};

int main(void) {
	/* Driver Object */
	ObjectSize driver;
	driver.calibrate();
	driver.computeSize();
	std::cout << "Program Completed";
	return 0;
}

